package api

import (
	"crypto/sha1"
	"felixa.com/golangmongod/pkg/api/auth"
	"felixa.com/golangmongod/pkg/api/conference"
	"felixa.com/golangmongod/pkg/api/presensi"
	"felixa.com/golangmongod/pkg/api/teacher"
	"felixa.com/golangmongod/pkg/api/test"
	"felixa.com/golangmongod/pkg/api/user"
	"felixa.com/golangmongod/pkg/utl/config"
	"felixa.com/golangmongod/pkg/utl/database/mongod"
	"felixa.com/golangmongod/pkg/utl/jwt"
	authMw "felixa.com/golangmongod/pkg/utl/middleware/auth"
	"felixa.com/golangmongod/pkg/utl/rbac"
	secure "felixa.com/golangmongod/pkg/utl/secure"
	"felixa.com/golangmongod/pkg/utl/server"

	tAuth "felixa.com/golangmongod/pkg/api/auth/transporter"
	tConference "felixa.com/golangmongod/pkg/api/conference/transporter"
	tPresensi "felixa.com/golangmongod/pkg/api/presensi/transporter"
	tTeacher "felixa.com/golangmongod/pkg/api/teacher/transporter"
	tTest "felixa.com/golangmongod/pkg/api/test/transporter"
	tUser "felixa.com/golangmongod/pkg/api/user/transporter"
)

func Start(cfg *config.Configuration) error {
	db, err := mongod.New(cfg.DB.Uri, cfg.DB.Timeout, cfg.DB.LogQueries)
	if err != nil {
		return err
	}

	sec := secure.New(cfg.App.MinPasswordStr, sha1.New())
	rbacService := rbac.Service{}
	jwtService, err := jwt.New(
		cfg.JWT.SigningAlgorithm,
		cfg.JWT.Secret,
		cfg.JWT.DurationMinutes,
		cfg.JWT.MinSecretLength,
		cfg.Server.ApiGatewayURL,
		cfg.Server.ApiGatewayConsumer,
	)
	if err != nil {
		return err
	}

	e := server.New()

	authMiddleware := authMw.Middleware(jwtService)

	tAuth.NewHTTP(auth.Initialize(db.Database("golang"), jwtService, rbacService, sec), e, authMiddleware)

	v1 := e.Group("/v1")
	v1.Use(authMiddleware)

	tUser.NewHTTP(user.Initialize(db.Database("golang"), sec), v1)
	tConference.NewHTTP(conference.Initialize(db.Database("golang"), rbacService), v1)
	tTest.NewHTTP(test.Initialize(db.Database("golang"), rbacService), v1)
	tPresensi.NewHTTP(presensi.Initialize(db.Database("golang"), rbacService), v1)
	tTeacher.NewHTTP(teacher.Initialize(db.Database("golang")), v1)

	server.Start(e, &server.Config{
		Port:                cfg.Server.Port,
		ReadTimeoutSeconds:  cfg.Server.ReadTimeout,
		WriteTimeoutSeconds: cfg.Server.WriteTimeout,
		Debug:               cfg.Server.Debug,
	})

	return nil
}
