package transporter

import (
	"felixa.com/golangmongod/entity"
)

type createReq struct {
	PresensiId               string `bson:"presensi_id" json:"presensi_id" validate:"required"`
	PresensiTopic            string `bson:"presensi_topic" json:"presensi_topic" validate:"required"`
	PresensiDate             string `bson:"presensi_date" json:"presensi_date" validate:"required"`
	PresensiTime             string `bson:"presensi_time" json:"presensi_time" validate:"required"`
	CountStudent             int    `bson:"count_student" json:"count_student" validate:"required"`
	CountPresentStudent      int    `bson:"count_present_student" json:"count_present_student" validate:"required"`
	CountAbsenStudent        int    `bson:"count_absen_student" json:"count_absen_student" validate:"required"`
	CountSickStudent         int    `bson:"count_sick_student" json:"count_sick_student" validate:"required"`
	CountPermitStudent       int    `bson:"count_permit_student" json:"count_permit_student" validate:"required"`
	CountDispensationStudent int    `bson:"count_dispensation_student" json:"count_dispensation_student" validate:"required"`
	SubjectName              string `bson:"subject_name" json:"subject_name" validate:"required"`
	SchoolClassName          string `bson:"school_class_name" json:"school_class_name" validate:"required"`
	Semester                 string `bson:"semester" json:"semester" validate:"required"`
	YearName                 string `bson:"year_name" json:"year_name" validate:"required"`
	TeacherName              string `bson:"teacher_name" json:"teacher_name" validate:"required"`
	TeacherUserName          string `bson:"teacher_user_name" json:"teacher_user_name" validate:"required"`
	SchoolName               string `bson:"school_name" json:"school_name" validate:"required"`
	AddressName              string `bson:"address_name" json:"address_name" validate:"required"`
	CountryName              string `bson:"country_name" json:"country_name" validate:"required"`
	ProvinceId               string `bson:"province_id" json:"province_id" validate:"required"`
	ProvinceName             string `bson:"province_name" json:"province_name" validate:"required"`
	DistrictId               string `bson:"district_id" json:"district_id" `
	DistrictName             string `bson:"district_name" json:"district_name"`
	SubDistrictId            string `bson:"subdistrict_id" json:"subdistrict_id"`
	SubDistrictName          string `bson:"subdistrict_name" json:"subdistrict_name"`
	VillageId                string `bson:"village_id" json:"village_id"`
	VillageName              string `bson:"village_name" json:"village_name"`
}
type createReqBulk struct {
	Data []createReq `json:"data" validate:"required"`
}

type listResponse struct {
	Presensis []entity.Presensi `json:"data" `
	Page      int64             `json:"page"`
}

// Function
func (c *createReq) ToPresensiEntity() entity.Presensi {
	return entity.Presensi{
		PresensiId:               c.PresensiId,
		PresensiTopic:            c.PresensiTopic,
		PresensiDate:             c.PresensiDate,
		PresensiTime:             c.PresensiTime,
		CountStudent:             c.CountStudent,
		CountPresentStudent:      c.CountPresentStudent,
		CountAbsenStudent:        c.CountAbsenStudent,
		CountSickStudent:         c.CountSickStudent,
		CountPermitStudent:       c.CountPermitStudent,
		CountDispensationStudent: c.CountDispensationStudent,
		SubjectName:              c.SubjectName,
		SchoolClassName:          c.SchoolClassName,
		Semester:                 c.Semester,
		YearName:                 c.YearName,
		TeacherName:              c.TeacherName,
		TeacherUserName:          c.TeacherUserName,
		SchoolName:               c.SchoolClassName,
		AddressName:              c.AddressName,
		CountryName:              c.CountryName,
		ProvinceId:               c.ProvinceId,
		ProvinceName:             c.ProvinceName,
		DistrictId:               c.DistrictId,
		DistrictName:             c.DistrictName,
		SubDistrictId:            c.SubDistrictId,
		SubDistrictName:          c.SubDistrictName,
		VillageId:                c.VillageId,
		VillageName:              c.VillageName,
	}
}
func (c *createReqBulk) ToArrayPresensiEntity() []entity.Presensi {
	var result []entity.Presensi
	for _, v := range c.Data {
		result = append(result, v.ToPresensiEntity())
	}
	return result
}
