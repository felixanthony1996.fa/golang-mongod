package presensi

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/api/presensi/mongod"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportData(echo.Context, string, string, string, string) (model.PresensiLiveReportData, error)
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportTableData(echo.Context, string, string, string, string) ([]model.PresensiLiveReportTableData, error)
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	Create(echo.Context, entity.Presensi) (entity.Presensi, error)
	CreateBulk(echo.Context, []entity.Presensi) ([]entity.Presensi, error)
	List(echo.Context, pagination.Pagination) ([]entity.Presensi, error)
	View(echo.Context, string) (entity.Presensi, error)
	Delete(echo.Context, string) error
}

type UDB interface {
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportData(*mongo.Collection, *entity.AccessControlQuery, string, string, string, string) ([][]int, error)
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportTableData(*mongo.Collection, *entity.AccessControlQuery, string, string, string, string) ([]model.PresensiLiveReportTableData, error)
	Create(*mongo.Collection, entity.Presensi) (entity.Presensi, error)
	List(*mongo.Collection, *entity.AccessControlQuery, pagination.Pagination) ([]entity.Presensi, error)
	Delete(*mongo.Collection, entity.Presensi) error
	DeleteByPresensiID(*mongo.Collection, entity.Presensi) error
	View(*mongo.Collection, string) (entity.Presensi, error)
}

type RBAC interface {
	User(ctx echo.Context) entity.AuthUser
}

type Presensi struct {
	collection *mongo.Collection
	udb        UDB
	rbac       RBAC
}

func Initialize(db *mongo.Database, rbac RBAC) *Presensi {
	return &Presensi{
		collection: db.Collection("presensi"),
		udb:        mongod.Presensi{},
		rbac:       rbac,
	}
}
