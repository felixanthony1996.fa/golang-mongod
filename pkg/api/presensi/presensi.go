package presensi

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/utl/accesscontrol"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
)

func (u Presensi) Create(c echo.Context, req entity.Presensi) (entity.Presensi, error) {
	return u.udb.Create(u.collection, req)
}

func (u Presensi) CreateBulk(c echo.Context, req []entity.Presensi) ([]entity.Presensi, error) {
	var result []entity.Presensi
	for _, v := range req {
		err := u.udb.DeleteByPresensiID(u.collection, v)
		resultDB, err := u.udb.Create(u.collection, v)
		if err != nil {
			return []entity.Presensi{}, err
		}
		result = append(result, resultDB)
	}
	return result, nil
}

func (u Presensi) List(c echo.Context, p pagination.Pagination) ([]entity.Presensi, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return nil, err
	}
	return u.udb.List(u.collection, acq, p)
}

func (u Presensi) Delete(c echo.Context, id string) error {
	modelDelete, err := u.udb.View(u.collection, id)
	if err != nil {
		return err
	}
	return u.udb.Delete(u.collection, modelDelete)
}

func (u Presensi) View(c echo.Context, id string) (entity.Presensi, error) {
	return u.udb.View(u.collection, id)
}

func (u Presensi) LiveReportData(c echo.Context, dari string, sampai string, kelas string, matapelajaran string) (model.PresensiLiveReportData, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return model.PresensiLiveReportData{}, err
	}
	result, err := u.udb.LiveReportData(u.collection, acq, dari, sampai, kelas, matapelajaran)
	if err != nil {
		return model.PresensiLiveReportData{}, err
	}

	return model.PresensiLiveReportData{
		Labels: []string{"Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"},
		Series: result,
	}, nil
}

func (u Presensi) LiveReportTableData(c echo.Context, dari string, sampai string, kelas string, matapelajaran string) ([]model.PresensiLiveReportTableData, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return []model.PresensiLiveReportTableData{}, err
	}
	result, err := u.udb.LiveReportTableData(u.collection, acq, dari, sampai, kelas, matapelajaran)
	if err != nil {
		return []model.PresensiLiveReportTableData{}, err
	}
	return result, nil
}
