package mongod

import (
	"context"
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Presensi struct{}

func (u Presensi) Create(col *mongo.Collection, model entity.Presensi) (entity.Presensi, error) {
	_, err := col.InsertOne(context.Background(), model)
	return model, err
}

func (u Presensi) List(col *mongo.Collection, acq *entity.AccessControlQuery, pagination pagination.Pagination) ([]entity.Presensi, error) {
	var results []entity.Presensi
	var filter bson.D = bson.D{}

	findOptions := options.Find()
	findOptions.SetLimit(pagination.Limit)
	if acq != nil {
		filter = append(filter, bson.E{acq.Query, acq.ID})
	}

	res, err := col.Find(context.TODO(), filter, findOptions)
	if err != nil {
		return nil, err
	}
	for res.Next(context.TODO()) {
		var e entity.Presensi
		err := res.Decode(&e)
		if err != nil {
			return nil, err
		}
		results = append(results, e)
	}
	return results, err
}

func (u Presensi) Delete(col *mongo.Collection, model entity.Presensi) error {
	_, err := col.DeleteOne(context.TODO(), bson.M{"_id": model.ID})
	return err
}

func (u Presensi) DeleteByPresensiID(col *mongo.Collection, model entity.Presensi) error {
	_, err := col.DeleteOne(context.TODO(), bson.M{"presensi_id": model.PresensiId})
	return err
}

func (u Presensi) View(col *mongo.Collection, _id string) (entity.Presensi, error) {
	var presensiModel entity.Presensi

	idPrimitive, err := primitive.ObjectIDFromHex(_id)
	if err != nil {
		return entity.Presensi{}, err
	}
	err = col.FindOne(context.TODO(), bson.M{"_id": idPrimitive}).Decode(&presensiModel)
	if err != nil {
		return entity.Presensi{}, err
	}
	return presensiModel, err
}

func (u Presensi) LiveReportData(col *mongo.Collection, acq *entity.AccessControlQuery, dari string, sampai string, kelas string, mataPelajaran string) ([][]int, error) {
	var data = [][]int{}
	var pipe = []bson.M{}
	// TODO SAMAKAN DENGAN CONFERENCE
	//if acq != nil {
	//	acqPipe := bson.M{"$match": bson.M{acq.Query: acq.ID}}
	//	pipe = append(pipe, acqPipe)
	//}

	// MATCH
	if dari != "" && sampai != "" {
		matchPipe := bson.M{
			"$match": bson.M{
				"presence_date": bson.M{
					"$gte": dari,
					"$lt":  sampai,
				},
			},
		}
		pipe = append(pipe, matchPipe)
	}
	if mataPelajaran != "" {
		matchPipe := bson.M{"$match": bson.M{
			"presence_topic": mataPelajaran,
		}}
		pipe = append(pipe, matchPipe)
	}
	if kelas != "" {
		matchPipe := bson.M{"$match": bson.M{
			"classroom_id": kelas,
		}}
		pipe = append(pipe, matchPipe)
	}

	// GROUPING
	groupPipe := bson.M{
		"$group": bson.M{
			"_id": bson.M{
				"dayOfWeek": bson.M{"$dayOfWeek": bson.M{"$toDate": "$presence_date"}},
			},
			"CountPresentStudent":      bson.M{"$sum": "$count_present_student"},
			"CountAbsenStudent":        bson.M{"$sum": "$count_absen_student"},
			"CountSickStudent":         bson.M{"$sum": "$count_sick_student"},
			"CountPermitStudent":       bson.M{"$sum": "$count_permit_student"},
			"CountDispensationStudent": bson.M{"$sum": "$count_dispensation_student"},
		},
	}
	pipe = append(pipe, groupPipe)

	// PROJECT
	projectPipe := bson.M{
		"$project": bson.M{
			"_id":                        0,
			"dayOfWeek":                  "$_id.dayOfWeek",
			"count_present_student":      "$CountPresentStudent",
			"count_absen_student":        "$CountAbsenStudent",
			"count_sick_student":         "$CountSickStudent",
			"count_permit_student":       "$CountPermitStudent",
			"count_dispensation_student": "$CountDispensationStudent",
		},
	}
	pipe = append(pipe, projectPipe)

	// SORTING
	sortPipe := bson.M{
		"$sort": bson.M{"dayOfWeek": 1},
	}
	pipe = append(pipe, sortPipe)

	// AGGREGATOR
	res, err := col.Aggregate(context.TODO(), pipe)
	if err != nil {
		return data, nil
	}

	// TRANSFORMATOR
	dataHadir, dataAbsen, dataSakit, dataIzin, dataDispensasi := []int{}, []int{}, []int{}, []int{}, []int{}
	for res.Next(context.TODO()) {
		var e entity.Presensi
		err := res.Decode(&e)
		if err != nil {
			return data, err
		}
		dataHadir = append(dataHadir, e.CountPresentStudent)
		dataAbsen = append(dataAbsen, e.CountAbsenStudent)
		dataSakit = append(dataSakit, e.CountSickStudent)
		dataIzin = append(dataIzin, e.CountPermitStudent)
		dataDispensasi = append(dataDispensasi, e.CountDispensationStudent)
	}
	data = append(data, dataHadir)
	data = append(data, dataAbsen)
	data = append(data, dataSakit)
	data = append(data, dataIzin)
	data = append(data, dataDispensasi)
	return data, nil
}

func (u Presensi) LiveReportTableData(col *mongo.Collection, acq *entity.AccessControlQuery, dari string, sampai string, kelas string, mataPelajaran string) ([]model.PresensiLiveReportTableData, error) {
	var pipe []bson.M
	var result []model.PresensiLiveReportTableData
	// TODO SAMAKAN DENGAN CONFERENCE
	//if acq != nil {
	//	acqPipe := bson.M{"$match": bson.M{acq.Query: acq.ID}}
	//	pipe = append(pipe, acqPipe)
	//}

	// MATCH
	if dari != "" && sampai != "" {
		matchPipe := bson.M{
			"$match": bson.M{
				"presence_date": bson.M{
					"$gte": dari,
					"$lt":  sampai,
				},
			},
		}
		pipe = append(pipe, matchPipe)
	}
	if mataPelajaran != "" {
		matchPipe := bson.M{"$match": bson.M{
			"presence_topic": mataPelajaran,
		}}
		pipe = append(pipe, matchPipe)
	}
	if kelas != "" {
		matchPipe := bson.M{"$match": bson.M{
			"classroom_id": kelas,
		}}
		pipe = append(pipe, matchPipe)
	}

	// GROUPING
	groupPipe := bson.M{
		"$group": bson.M{
			"_id": primitive.ObjectID{},
			"AvgPresent": bson.M{
				"$sum": "$count_present_student",
			},
			"AvgAbsent": bson.M{
				"$sum": "$count_absen_student",
			},
			"AvgSick": bson.M{
				"$sum": "$count_sick_student",
			},
			"AvgPermit": bson.M{
				"$sum": "$count_permit_student",
			},
			"AvgDispensation": bson.M{
				"$sum": "$count_dispensation_student",
			},
			"TotalPresensi": bson.M{
				"$sum": bson.M{
					"$add": bson.A{"$count_present_student", "$count_absen_student", "$count_sick_student", "$count_permit_student", "$count_dispensation_student"},
				},
			},
		},
	}
	pipe = append(pipe, groupPipe)

	// PROJECT
	projectPipe := bson.M{
		"$project": bson.M{
			"TotalPresensi": "$TotalPresensi",
			"AvgPresent": bson.M{
				"$round": bson.M{
					"$multiply": bson.A{
						bson.M{"$divide": bson.A{"$AvgPresent", "$TotalPresensi"}},
						100,
					},
				},
			},
			"AvgAbsent": bson.M{
				"$round": bson.M{
					"$multiply": bson.A{
						bson.M{"$divide": bson.A{"$AvgAbsent", "$TotalPresensi"}},
						100,
					},
				},
			},
			"AvgSick": bson.M{
				"$round": bson.M{
					"$multiply": bson.A{
						bson.M{"$divide": bson.A{"$AvgSick", "$TotalPresensi"}},
						100,
					},
				},
			},
			"AvgPermit": bson.M{
				"$round": bson.M{
					"$multiply": bson.A{
						bson.M{"$divide": bson.A{"$AvgPermit", "$TotalPresensi"}},
						100,
					},
				},
			},
			"AvgDispensation": bson.M{
				"$round": bson.M{
					"$multiply": bson.A{
						bson.M{"$divide": bson.A{"$AvgDispensation", "$TotalPresensi"}},
						100,
					},
				},
			},
		},
	}
	pipe = append(pipe, projectPipe)

	res, err := col.Aggregate(context.TODO(), pipe)
	if err != nil {
		return []model.PresensiLiveReportTableData{}, nil
	}

	// TRANSFORMATOR
	res.Next(context.TODO())
	var e entity.Presensi
	err = res.Decode(&e)
	if err != nil {
		return []model.PresensiLiveReportTableData{}, err
	}
	result = append(result, model.PresensiLiveReportTableData{
		Key:    "1",
		Type:   "Rata-rata Kehadiran",
		Jumlah: e.AvgPresent,
	})
	result = append(result, model.PresensiLiveReportTableData{
		Key:    "2",
		Type:   "Rata-rata Alpa",
		Jumlah: e.AvgAbsent,
	})
	result = append(result, model.PresensiLiveReportTableData{
		Key:    "3",
		Type:   "Rata-rata Sakit",
		Jumlah: e.AvgSick,
	})
	result = append(result, model.PresensiLiveReportTableData{
		Key:    "4",
		Type:   "Rata-rata Izin",
		Jumlah: e.AvgPermit,
	})
	result = append(result, model.PresensiLiveReportTableData{
		Key:    "5",
		Type:   "Rata-rata Dipensasi",
		Jumlah: e.AvgDispensation,
	})
	return result, nil
}
