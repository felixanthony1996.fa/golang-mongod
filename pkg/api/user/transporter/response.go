package transporter

import "felixa.com/golangmongod/entity"

type createReq struct {
	UUID       string `json:"uuid" validate:"required"`
	Name       string `json:"name" validate:"required"`
	Username   string `json:"username" validate:"required,min=3,alphanum"`
	Password   string `json:"password" validate:"required,min=3,alphanum"`
	Email      string `json:"email" validate:"required,email"`
	Role       int    `json:"role" validate:"required,min=3,numeric"`
	ProvinceID string `json:"province_id" validate:"required"`
}

type listResponse struct {
	Users []entity.User `json:"data"`
	Page  int64         `json:"page"`
}
