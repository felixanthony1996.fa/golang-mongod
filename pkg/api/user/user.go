package user

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
)

func (u User) Create(c echo.Context, req entity.User) (entity.User, error) {
	req.Password = u.sec.Hash(req.Password)
	return u.udb.Create(u.collection, req)
}

func (u User) List(c echo.Context, p pagination.Pagination) ([]entity.User, error) {
	return u.udb.List(u.collection, p)
}
