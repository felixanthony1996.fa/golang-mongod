package logging

import "felixa.com/golangmongod/pkg/api/user"

func New(svc user.Service) *LogService {
	return &LogService{
		Service: svc,
	}
}

type LogService struct {
	user.Service
}
