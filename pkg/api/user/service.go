package user

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/api/user/mongod"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	Create(echo.Context, entity.User) (entity.User, error)
	List(echo.Context, pagination.Pagination) ([]entity.User, error)
}

type UDB interface {
	Create(*mongo.Collection, entity.User) (entity.User, error)
	List(*mongo.Collection, pagination.Pagination) ([]entity.User, error)
}
type Securer interface {
	Hash(string) string
}

type User struct {
	collection *mongo.Collection
	udb        UDB
	sec        Securer
}

func Initialize(db *mongo.Database,securer Securer) *User {
	return &User{
		collection: db.Collection("user"),
		udb: mongod.User{},
		sec: securer,
	}
}
