package mongod

import (
	"context"
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type User struct{}

func (u User) Create(col *mongo.Collection, model entity.User) (entity.User, error) {
	model.ID = primitive.NewObjectID()
	_, err := col.InsertOne(context.Background(), model)
	return model, err
}

func (u User) List(col *mongo.Collection, pagination pagination.Pagination) ([]entity.User, error) {
	var results []entity.User

	findOptions := options.Find()
	findOptions.SetLimit(pagination.Limit)
	res, err := col.Find(context.TODO(), bson.D{}, findOptions)
	if err != nil {
		return nil, err
	}
	for res.Next(context.TODO()) {
		var e entity.User
		err := res.Decode(&e)
		if err != nil {
			return nil, err
		}
		results = append(results, e)
	}
	return results, err
}
