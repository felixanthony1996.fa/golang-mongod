package mongod

import (
	"context"
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Test struct{}

func (u Test) Create(col *mongo.Collection, model entity.Test) (entity.Test, error) {
	_, err := col.InsertOne(context.Background(), model)
	return model, err
}

func (u Test) List(col *mongo.Collection, acq *entity.AccessControlQuery, pagination pagination.Pagination) ([]entity.Test, error) {
	var results []entity.Test
	var filter bson.D = bson.D{}

	findOptions := options.Find()
	findOptions.SetLimit(pagination.Limit)
	if acq != nil {
		filter = append(filter, bson.E{acq.Query, acq.ID})
	}

	res, err := col.Find(context.TODO(), filter, findOptions)
	if err != nil {
		return nil, err
	}
	for res.Next(context.TODO()) {
		var e entity.Test
		err := res.Decode(&e)
		if err != nil {
			return nil, err
		}
		results = append(results, e)
	}
	return results, err
}

func (u Test) Delete(col *mongo.Collection, model entity.Test) error {
	_, err := col.DeleteOne(context.TODO(), bson.M{"_id": model.ID})
	return err
}

func (u Test) DeleteByTestID(col *mongo.Collection, model entity.Test) error {
	_, err := col.DeleteOne(context.TODO(), bson.M{"test_id": model.TestId})
	return err
}

func (u Test) View(col *mongo.Collection, _id string) (entity.Test, error) {
	var testModel entity.Test

	idPrimitive, err := primitive.ObjectIDFromHex(_id)
	if err != nil {
		return entity.Test{}, err
	}
	err = col.FindOne(context.TODO(), bson.M{"_id": idPrimitive}).Decode(&testModel)
	if err != nil {
		return entity.Test{}, err
	}
	return testModel, err
}

func (u Test) LiveReportData(col *mongo.Collection, acq *entity.AccessControlQuery, dari string, sampai string, kelas string, mataPelajaran string) ([][]int, error) {
	var data = [][]int{}
	var pipe = []bson.M{}
	// TODO SAMAKAN DENGAN CONFERENCE
	//if acq != nil {
	//	acqPipe := bson.M{"$match": bson.M{acq.Query: acq.ID}}
	//	pipe = append(pipe, acqPipe)
	//}

	// MATCH
	if dari != "" && sampai != "" {
		matchPipe := bson.M{
			"$match": bson.M{
				"test_start": bson.M{
					"$gte": dari,
					"$lt":  sampai,
				},
			},
		}
		pipe = append(pipe, matchPipe)
	}
	if mataPelajaran != "" {
		matchPipe := bson.M{"$match": bson.M{
			"test_topic": mataPelajaran,
		}}
		pipe = append(pipe, matchPipe)
	}
	if kelas != "" {
		matchPipe := bson.M{"$match": bson.M{
			"classroom_id": kelas,
		}}
		pipe = append(pipe, matchPipe)
	}

	// GROUPING
	groupPipe := bson.M{
		"$group": bson.M{
			"_id": bson.M{
				"dayOfWeek": bson.M{"$dayOfWeek": bson.M{"$toDate": "$test_start"}},
			},
			"count": bson.M{"$sum": 1},
		},
	}
	pipe = append(pipe, groupPipe)

	// PROJECT
	projectPipe := bson.M{
		"$project": bson.M{
			"_id":       0,
			"dayOfWeek": "$_id.dayOfWeek",
			"count":     "$count",
		},
	}
	pipe = append(pipe, projectPipe)

	// SORTING
	sortPipe := bson.M{
		"$sort": bson.M{"dayOfWeek": 1},
	}
	pipe = append(pipe, sortPipe)

	// AGGREGATOR
	res, err := col.Aggregate(context.TODO(), pipe)
	if err != nil {
		return data, nil
	}

	// TRANSFORMATOR
	var dataDetail = []int{}
	for res.Next(context.TODO()) {
		var e entity.Test
		err := res.Decode(&e)
		if err != nil {
			return data, err
		}
		dataDetail = append(dataDetail, e.Count)
		log.Print("FelX", e.Count)
	}
	data = append(data, dataDetail)
	return data, nil
}

func (u Test) LiveReportAverageData(col *mongo.Collection, acq *entity.AccessControlQuery, dari string, sampai string, kelas string, mataPelajaran string) (model.TestAverageLiveReportDataset, error) {
	var pipe []bson.M
	var label []string
	var data []int
	var datasets []model.TestAverageLiveReportDataDetail
	// TODO SAMAKAN DENGAN CONFERENCE
	//if acq != nil {
	//	acqPipe := bson.M{"$match": bson.M{acq.Query: acq.ID}}
	//	pipe = append(pipe, acqPipe)
	//}

	// MATCH
	if dari != "" && sampai != "" {
		matchPipe := bson.M{
			"$match": bson.M{
				"test_start": bson.M{
					"$gte": dari,
					"$lt":  sampai,
				},
			},
		}
		pipe = append(pipe, matchPipe)
	}
	if mataPelajaran != "" {
		matchPipe := bson.M{"$match": bson.M{
			"subject_name": mataPelajaran,
		}}
		pipe = append(pipe, matchPipe)
	}
	if kelas != "" {
		matchPipe := bson.M{"$match": bson.M{
			"classroom_id": kelas,
		}}
		pipe = append(pipe, matchPipe)
	}

	// GROUPING
	groupPipe := bson.M{
		"$group": bson.M{
			"_id": bson.M{"subject_name": "$subject_name"},
			"test_range": bson.M{
				"$avg": "$test_range",
			},
		},
	}
	pipe = append(pipe, groupPipe)

	// PROJECT
	projectPipe := bson.M{
		"$project": bson.M{
			"_id":          0,
			"subject_name": "$_id.subject_name",
			"test_range": bson.M{
				"$toInt": "$test_range",
			},
		},
	}
	pipe = append(pipe, projectPipe)

	// SORTING
	sortPipe := bson.M{
		"$sort": bson.M{"subject_name": 1},
	}
	pipe = append(pipe, sortPipe)

	// AGGREGATOR
	res, err := col.Aggregate(context.TODO(), pipe)
	if err != nil {
		return model.TestAverageLiveReportDataset{}, nil
	}

	for res.Next(context.TODO()) {
		var e entity.Test
		err := res.Decode(&e)
		if err != nil {
			return model.TestAverageLiveReportDataset{}, err
		}
		label = append(label, e.SubjectName)
		data = append(data, e.TestRange)
		log.Print("FelX", e.SubjectName, e.TestRange)
	}

	datasets = append(datasets, model.TestAverageLiveReportDataDetail{
		Labels: "Rata rata nilai per mata pelajaran",
		Data:   data,
	})
	return model.TestAverageLiveReportDataset{Labels: label, Datasets: datasets}, nil
}
