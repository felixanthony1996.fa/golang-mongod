package test

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/utl/accesscontrol"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
)

func (u Test) Create(c echo.Context, req entity.Test) (entity.Test, error) {
	return u.udb.Create(u.collection, req)
}

func (u Test) CreateBulk(c echo.Context, req []entity.Test) ([]entity.Test, error) {
	var result []entity.Test
	for _, v := range req {
		err := u.udb.DeleteByTestID(u.collection, v)
		resultDB, err := u.udb.Create(u.collection, v)
		if err != nil {
			return []entity.Test{}, err
		}
		result = append(result, resultDB)
	}
	return result, nil
}

func (u Test) List(c echo.Context, p pagination.Pagination) ([]entity.Test, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return nil, err
	}
	return u.udb.List(u.collection, acq, p)
}

func (u Test) Delete(c echo.Context, id string) error {
	model, err := u.udb.View(u.collection, id)
	if err != nil {
		return err
	}
	return u.udb.Delete(u.collection, model)
}

func (u Test) View(c echo.Context, id string) (entity.Test, error) {
	return u.udb.View(u.collection, id)
}

func (u Test) LiveReportData(c echo.Context, dari string, sampai string, kelas string, matapelajaran string) (model.TestLiveReportData, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return model.TestLiveReportData{}, err
	}
	result, err := u.udb.LiveReportData(u.collection, acq, dari, sampai, kelas, matapelajaran)
	if err != nil {
		return model.TestLiveReportData{}, err
	}

	return model.TestLiveReportData{
		Labels: []string{"Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"},
		Series: result,
	}, nil
}

func (u Test) LiveReportAverageData(c echo.Context, dari string, sampai string, kelas string, matapelajaran string) (model.TestAverageLiveReportDataset, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return model.TestAverageLiveReportDataset{}, err
	}
	result, err := u.udb.LiveReportAverageData(u.collection, acq, dari, sampai, kelas, matapelajaran)
	if err != nil {
		return model.TestAverageLiveReportDataset{}, err
	}
	return result, err
}
