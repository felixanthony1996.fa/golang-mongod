package test

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/api/test/mongod"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportData(echo.Context, string, string, string, string) (model.TestLiveReportData, error)
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportAverageData(echo.Context, string, string, string, string) (model.TestAverageLiveReportDataset, error)
	Create(echo.Context, entity.Test) (entity.Test, error)
	CreateBulk(echo.Context, []entity.Test) ([]entity.Test, error)
	List(echo.Context, pagination.Pagination) ([]entity.Test, error)
	View(echo.Context, string) (entity.Test, error)
	Delete(echo.Context, string) error
}

type UDB interface {
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportData(*mongo.Collection, *entity.AccessControlQuery, string, string, string, string) ([][]int, error)
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportAverageData(*mongo.Collection, *entity.AccessControlQuery, string, string, string, string) (model.TestAverageLiveReportDataset, error)
	Create(*mongo.Collection, entity.Test) (entity.Test, error)
	List(*mongo.Collection, *entity.AccessControlQuery, pagination.Pagination) ([]entity.Test, error)
	Delete(*mongo.Collection, entity.Test) error
	DeleteByTestID(*mongo.Collection, entity.Test) error
	View(*mongo.Collection, string) (entity.Test, error)
}

type RBAC interface {
	User(ctx echo.Context) entity.AuthUser
}

type Test struct {
	collection *mongo.Collection
	udb        UDB
	rbac       RBAC
}

func Initialize(db *mongo.Database, rbac RBAC) *Test {
	return &Test{
		collection: db.Collection("test"),
		udb:        mongod.Test{},
		rbac:       rbac,
	}
}
