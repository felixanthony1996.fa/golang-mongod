package transporter

import (
	"felixa.com/golangmongod/entity"
)

type createReq struct {
	TestId          string `bson:"test_id" json:"test_id" validate:"required"`
	TestTopic       string `bson:"test_topic" json:"test_topic" validate:"required"`
	TestStart       string `bson:"test_start" json:"test_start"`
	TestRange       int    `bson:"test_range" json:"test_range" validate:"required"`
	CountExaminee   int    `bson:"count_examinee" json:"count_examinee" validate:"required"`
	MinScore        int    `bson:"min_score" json:"min_score"`
	MaxScore        int    `bson:"max_score" json:"max_score"`
	AvgScore        int    `bson:"avg_score" json:"avg_score"`
	SubjectName     string `bson:"subject_name" json:"subject_name" validate:"required"`
	SchoolClassName string `bson:"school_class_name" json:"school_class_name" validate:"required"`
	LevelName       int    `bson:"level_name" json:"level_name" validate:"required"`
	MajorName       string `bson:"major_name" json:"major_name" validate:"required"`
	Semester        string `bson:"semester" json:"semester" validate:"required"`
	YearName        string `bson:"year_name" json:"year_name" validate:"required"`
	TeacherName     string `bson:"teacher_name" json:"teacher_name" validate:"required"`
	TeacherUserName string `bson:"teacher_user_name" json:"teacher_user_name" validate:"required"`
	SchoolName      string `bson:"school_name" json:"school_name" validate:"required"`
	StageName       string `bson:"stage_name" json:"stage_name" validate:"required"`
	AddressName     string `bson:"address_name" json:"address_name" validate:"required"`
	Country         string `bson:"country" json:"country" validate:"required"`
	Province        string `bson:"province" json:"province" validate:"required"`
	District        string `bson:"district" json:"district" validate:"required"`
	Subdistrict     string `bson:"subdistrict" json:"subdistrict" validate:"required"`
	Village         string `bson:"village" json:"village" validate:"required"`
}
type createReqBulk struct {
	Data []createReq `json:"data" validate:"required"`
}

type listResponse struct {
	Tests []entity.Test `json:"data" `
	Page  int64         `json:"page"`
}

// Function
func (c *createReq) ToTestEntity() entity.Test {
	return entity.Test{
		TestId:          c.TestId,
		TestTopic:       c.TestTopic,
		TestStart:       c.TestStart,
		TestRange:       c.TestRange,
		CountExaminee:   c.CountExaminee,
		MinScore:        c.MinScore,
		MaxScore:        c.MaxScore,
		AvgScore:        c.AvgScore,
		SubjectName:     c.SubjectName,
		SchoolClassName: c.SchoolClassName,
		LevelName:       c.LevelName,
		MajorName:       c.MajorName,
		Semester:        c.Semester,
		YearName:        c.YearName,
		TeacherName:     c.TeacherName,
		TeacherUserName: c.TeacherUserName,
		SchoolName:      c.SchoolClassName,
		StageName:       c.StageName,
		AddressName:     c.AddressName,
		Country:         c.Country,
		Province:        c.Province,
		District:        c.District,
		Subdistrict:     c.Subdistrict,
		Village:         c.Village,
	}
}
func (c *createReqBulk) ToArrayTestEntity() []entity.Test {
	var result []entity.Test
	for _, v := range c.Data {
		result = append(result, v.ToTestEntity())
	}
	return result
}
