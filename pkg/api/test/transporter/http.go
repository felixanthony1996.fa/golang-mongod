package transporter

import (
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/api/test"
	"felixa.com/golangmongod/pkg/utl/errorhandler"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
	"net/http"
)

type HTTP struct {
	svc test.Service
}

type liveReport struct {
	Dari          string `json:"dari" validate:"required"`
	Sampai        string `json:"sampai" validate:"required"`
	Kelas         string `json:"kelas"`
	MataPelajaran string `json:"mata_pelajaran"`
}

// HTTP SERVER
func NewHTTP(svc test.Service, r *echo.Group) {
	h := HTTP{svc}
	ur := r.Group("/test")

	// Route
	ur.POST("/live-report", h.liveReport)
	ur.POST("/live-report-by-average-mapel", h.liveReportByAverageMapel)
	ur.POST("/create", h.create)
	ur.POST("/create-bulk", h.createBulk)
	ur.GET("/index", h.list)
	ur.GET("/:id", h.view)
	ur.DELETE("/:id", h.delete)
}

// PROCESS HTTP REQUEST
func (h *HTTP) create(c echo.Context) error {
	r := new(createReq)
	if err := c.Bind(r); err != nil {

		return err
	}
	usr, err := h.svc.Create(c, r.ToTestEntity())
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

func (h *HTTP) createBulk(c echo.Context) error {
	r := new(createReqBulk)
	if err := c.Bind(r); err != nil {
		return err
	}
	usr, err := h.svc.CreateBulk(c, r.ToArrayTestEntity())
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

func (h *HTTP) list(c echo.Context) error {
	var req pagination.PaginationReq
	if err := c.Bind(&req); err != nil {
		return err
	}
	result, err := h.svc.List(c, req.Transform())
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, listResponse{result, req.Page})
}

func (h *HTTP) liveReport(c echo.Context) error {
	liveReport := new(liveReport)
	if err := c.Bind(liveReport); err != nil {
		return err
	}
	resultData, err := h.svc.LiveReportData(c, liveReport.Dari, liveReport.Sampai, liveReport.Kelas, liveReport.MataPelajaran)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, model.TestLiveReport{Data: resultData})
}

func (h *HTTP) liveReportByAverageMapel(c echo.Context) error {
	liveReport := new(liveReport)
	if err := c.Bind(liveReport); err != nil {
		return err
	}
	resultData, err := h.svc.LiveReportAverageData(c, liveReport.Dari, liveReport.Sampai, liveReport.Kelas, liveReport.MataPelajaran)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, model.TestAverageLiveReport{Data: resultData})
}

func (h *HTTP) view(c echo.Context) error {
	id := c.Param("id")
	if id == "" {
		return errorhandler.ErrBadRequest
	}
	result, err := h.svc.View(c, id)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, result)
}

func (h HTTP) delete(c echo.Context) error {
	id := c.Param("id")
	if id == "" {
		return errorhandler.ErrBadRequest
	}

	if err := h.svc.Delete(c, id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}
