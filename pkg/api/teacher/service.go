package teacher

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/api/teacher/mongod"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	Create(echo.Context, entity.Teacher) (entity.Teacher, error)
	List(echo.Context, pagination.Pagination) ([]entity.Teacher, error)
}

type UDB interface {
	Create(*mongo.Collection, entity.Teacher) (entity.Teacher, error)
	List(*mongo.Collection, pagination.Pagination) ([]entity.Teacher, error)
}

type Teacher struct {
	collection *mongo.Collection
	udb        UDB
}

func Initialize(db *mongo.Database) *Teacher {
	return &Teacher{collection: db.Collection("teacher"), udb: mongod.Teacher{}}
}
