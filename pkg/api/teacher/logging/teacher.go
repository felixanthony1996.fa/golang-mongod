package logging

import (
	"felixa.com/golangmongod/pkg/api/teacher"
)

func New(svc teacher.Service) *LogService {
	return &LogService{
		Service: svc,
	}
}

type LogService struct {
	teacher.Service
}
