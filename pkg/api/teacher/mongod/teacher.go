package mongod

import (
	"context"
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Teacher struct{}

func (u Teacher) Create(col *mongo.Collection, model entity.Teacher) (entity.Teacher, error) {
	result, err := col.InsertOne(context.Background(), model)

	model.ID = result.InsertedID.(primitive.ObjectID)

	return model, err
}

func (u Teacher) List(col *mongo.Collection, pagination pagination.Pagination) ([]entity.Teacher, error) {
	var results []entity.Teacher

	findOptions := options.Find()
	findOptions.SetLimit(pagination.Limit)
	res, err := col.Find(context.TODO(), bson.D{}, findOptions)
	if err != nil {
		return nil, err
	}
	for res.Next(context.TODO()) {
		var e entity.Teacher
		err := res.Decode(&e)
		if err != nil {
			return nil, err
		}
		results = append(results, e)
	}
	return results, err
}
