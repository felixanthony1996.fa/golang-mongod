package transporter

import "felixa.com/golangmongod/entity"

type createReq struct {
	Todo     string `json:"name" validate:"required"`
}

type listResponse struct {
	Teachers []entity.Teacher `json:"data"`
	Page  int64         `json:"page"`
}
