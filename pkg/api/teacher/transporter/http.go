package transporter

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/api/teacher"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
	"net/http"
)

type HTTP struct {
	svc teacher.Service
}

// HTTP SERVER
func NewHTTP(svc teacher.Service, r *echo.Group) {
	h := HTTP{svc}
	ur := r.Group("/teachers")

	// Route
	ur.POST("", h.create)
	ur.GET("", h.list)
}

// PROCESS HTTP REQUEST
func (h *HTTP) create(c echo.Context) error {
	r := new(createReq)

	if err := c.Bind(r); err != nil {

		return err
	}

	usr, err := h.svc.Create(c, entity.Teacher{
		Todo: r.Todo,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

func (h *HTTP) list(c echo.Context) error {
	var req pagination.PaginationReq
	if err := c.Bind(&req); err != nil {
		return err
	}
	result, err := h.svc.List(c, req.Transform())
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, listResponse{result, req.Page})
}
