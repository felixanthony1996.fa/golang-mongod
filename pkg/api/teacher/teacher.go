package teacher

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
)

func (u Teacher) Create(c echo.Context, req entity.Teacher) (entity.Teacher, error) {
	return u.udb.Create(u.collection, req)
}

func (u Teacher) List(c echo.Context, p pagination.Pagination) ([]entity.Teacher, error) {
	return u.udb.List(u.collection, p)
}
