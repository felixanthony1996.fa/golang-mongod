package auth

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/api/auth/mongod"
	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	Authenticate(echo.Context, string, string) (entity.AuthToken, error)
	Whoami(echo.Context) (entity.User, error)
}

type UDB interface {
	FindByUsername(*mongo.Collection, string) (entity.User, error)
	Update(*mongo.Collection, entity.User) error
	View(*mongo.Collection, string) (entity.User, error)
}

type TokenGenerator interface {
	GenerateToken(user entity.User, key string) (string, error)
	GenerateKeyFromGateway(user entity.User) (string, error)
}

type RBAC interface {
	User(echo.Context) entity.AuthUser
}

type Securer interface {
	HashMatchesPassword(string, string) bool
	Token(string) string
}

type Auth struct {
	collection     *mongo.Collection
	udb            UDB
	tokenGenerator TokenGenerator
	sec            Securer
	rbac           RBAC
}

func Initialize(db *mongo.Database, jwt TokenGenerator, rbac RBAC, sec Securer) Auth {
	return Auth{
		collection:     db.Collection("user"),
		udb:            mongod.Auth{},
		tokenGenerator: jwt,
		rbac:           rbac,
		sec:            sec,
	}
}
