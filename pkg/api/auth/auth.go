package auth

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/utl/errorhandler"
	"github.com/labstack/echo"
	"net/http"
)

var (
	ErrInvalidCredentials  = echo.NewHTTPError(http.StatusUnauthorized, "Username or password does not exist")
	ErrFailedGetKeyGateway = echo.NewHTTPError(http.StatusUnauthorized, "Cant get Key from API Gateway")
)

func (a Auth) Authenticate(c echo.Context, user string, pass string) (entity.AuthToken, error) {
	u, err := a.udb.FindByUsername(a.collection, user)
	if err != nil {
		return entity.AuthToken{}, err
	}
	if !u.Active {
		return entity.AuthToken{}, errorhandler.ErrUnauthorized
	}

	if !a.sec.HashMatchesPassword(u.Password, pass) {
		return entity.AuthToken{}, ErrInvalidCredentials
	}

	key, err := a.tokenGenerator.GenerateKeyFromGateway(u)
	if key == "" {
		return entity.AuthToken{}, ErrFailedGetKeyGateway
	}

	token, err := a.tokenGenerator.GenerateToken(u, key)
	if err != nil {
		return entity.AuthToken{}, ErrInvalidCredentials
	}

	u.UpdateLastLogin(token)

	if err := a.udb.Update(a.collection, u); err != nil {
		return entity.AuthToken{}, err
	}

	return entity.AuthToken{Token: token}, nil
}

func (a Auth) Whoami(c echo.Context) (entity.User, error) {
	au := a.rbac.User(c)
	return a.udb.View(a.collection, au.ID)
}
