package mongod

import (
	"context"
	"felixa.com/golangmongod/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type Auth struct{}

func (a Auth) FindByUsername(col *mongo.Collection, username string) (entity.User, error) {
	var result entity.User

	filter := bson.D{{"username", username}}

	err := col.FindOne(context.TODO(), filter).Decode(&result)
	return result, err
}

func (a Auth) Update(col *mongo.Collection, model entity.User) error {
	filter := bson.D{{"username", model.Username}}
	update := bson.M{
		"$set": bson.M{
			"last_login": model.LastLogin,
		},
	}

	_, err := col.UpdateOne(context.TODO(), filter, update)
	return err
}

func (a Auth) View(col *mongo.Collection, id string) (entity.User, error) {
	var result entity.User

	filter := bson.D{{"uuid", id}}

	err := col.FindOne(context.TODO(), filter).Decode(&result)
	return result, err
}
