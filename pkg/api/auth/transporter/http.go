package transporter

import (
	"felixa.com/golangmongod/pkg/api/auth"
	"github.com/labstack/echo"
	"net/http"
)

type HTTP struct {
	svc auth.Service
}

type credentials struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

func NewHTTP(svc auth.Service, e *echo.Echo, mw echo.MiddlewareFunc) {
	h := HTTP{svc}

	e.POST("/login", h.login)
	e.GET("/whoami", h.whoami, mw)
}

func (h *HTTP) login(c echo.Context) error {
	credential := new(credentials)
	if err := c.Bind(credential); err != nil {
		return nil
	}
	res, err := h.svc.Authenticate(c, credential.Username, credential.Password)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, res)
}

func (h *HTTP) whoami(c echo.Context) error {
	user, err := h.svc.Whoami(c)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, user)
}
