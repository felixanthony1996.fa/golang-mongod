package mongod

import (
	"context"
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Conference struct{}

func (u Conference) Create(col *mongo.Collection, model entity.Conference) (entity.Conference, error) {
	_, err := col.InsertOne(context.Background(), model)
	return model, err
}

func (u Conference) List(col *mongo.Collection, acq *entity.AccessControlQuery, pagination pagination.Pagination) ([]entity.Conference, error) {
	var results []entity.Conference
	var filter bson.D = bson.D{}

	findOptions := options.Find()
	findOptions.SetLimit(pagination.Limit)
	if acq != nil {
		filter = append(filter, bson.E{acq.Query, acq.ID})
	}

	res, err := col.Find(context.TODO(), filter, findOptions)
	if err != nil {
		return nil, err
	}
	for res.Next(context.TODO()) {
		var e entity.Conference
		err := res.Decode(&e)
		if err != nil {
			return nil, err
		}
		results = append(results, e)
	}
	return results, err
}

func (u Conference) Delete(col *mongo.Collection, model entity.Conference) error {
	_, err := col.DeleteOne(context.TODO(), bson.M{"_id": model.ID})
	return err
}

func (u Conference) DeleteByConferenceID(col *mongo.Collection, model entity.Conference) error {
	_, err := col.DeleteOne(context.TODO(), bson.M{"conference_id": model.ConferenceId})
	return err
}

func (u Conference) View(col *mongo.Collection, _id string) (entity.Conference, error) {
	var model entity.Conference

	idPrimitive, err := primitive.ObjectIDFromHex(_id)
	if err != nil {
		return entity.Conference{}, err
	}
	err = col.FindOne(context.TODO(), bson.M{"_id": idPrimitive}).Decode(&model)
	if err != nil {
		return entity.Conference{}, err
	}
	return model, err
}

func (u Conference) LiveReportData(col *mongo.Collection, acq *entity.AccessControlQuery, dari string, sampai string, kelas string, mataPelajaran string) (model.ConferenceLiveReportDataDetail, error) {
	var data = []int{}
	var pipe = []bson.M{}
	if acq != nil {
		acqPipe := bson.M{"$match": bson.M{acq.Query: acq.ID}}
		pipe = append(pipe, acqPipe)
	}

	// MATCH
	if dari != "" && sampai != "" {
		matchPipe := bson.M{
			"$match": bson.M{
				"conference_start": bson.M{
					"$gte": dari,
					"$lt":  sampai,
				},
			},
		}
		pipe = append(pipe, matchPipe)
	}
	if mataPelajaran != "" {
		matchPipe := bson.M{"$match": bson.M{
			"conference_topic": mataPelajaran,
		}}
		pipe = append(pipe, matchPipe)
	}
	if kelas != "" {
		matchPipe := bson.M{"$match": bson.M{
			"classroom_id": kelas,
		}}
		pipe = append(pipe, matchPipe)
	}

	// GROUPING
	groupPipe := bson.M{
		"$group": bson.M{
			"_id": bson.M{
				"dayOfWeek": bson.M{"$dayOfWeek": bson.M{"$toDate": "$conference_start"}},
			},
			"count": bson.M{"$sum": 1},
		},
	}
	pipe = append(pipe, groupPipe)

	// PROJECT
	projectPipe := bson.M{
		"$project": bson.M{
			"_id":       0,
			"dayOfWeek": "$_id.dayOfWeek",
			"count":     "$count",
		},
	}
	pipe = append(pipe, projectPipe)

	// SORTING
	sortPipe := bson.M{
		"$sort": bson.M{"dayOfWeek": 1},
	}
	pipe = append(pipe, sortPipe)

	// AGGREGATOR
	res, err := col.Aggregate(context.TODO(), pipe)
	if err != nil {
		return model.ConferenceLiveReportDataDetail{}, nil
	}

	// TRANSFORMATOR
	for res.Next(context.TODO()) {
		var e entity.Conference
		err := res.Decode(&e)
		if err != nil {
			return model.ConferenceLiveReportDataDetail{Labels: "Eror"}, err
		}
		data = append(data, e.Count)
	}
	return model.ConferenceLiveReportDataDetail{Labels: "Total Video Conference", Data: data}, nil
}

func (u Conference) LiveReportDataByMapel(col *mongo.Collection, acq *entity.AccessControlQuery, dari string, sampai string, kelas string, mataPelajaran string) (model.ConferenceLiveReportDataset, error) {
	var pipe []bson.M
	var label []string
	var data []int
	var datasets []model.ConferenceLiveReportDataDetail
	if acq != nil {
		acqPipe := bson.M{"$match": bson.M{acq.Query: acq.ID}}
		pipe = append(pipe, acqPipe)
	}

	// MATCH
	if dari != "" && sampai != "" {
		matchPipe := bson.M{
			"$match": bson.M{
				"conference_start": bson.M{
					"$gte": dari,
					"$lt":  sampai,
				},
			},
		}
		pipe = append(pipe, matchPipe)
	}
	if mataPelajaran != "" {
		matchPipe := bson.M{"$match": bson.M{
			"conference_topic": mataPelajaran,
		}}
		pipe = append(pipe, matchPipe)
	}
	if kelas != "" {
		matchPipe := bson.M{"$match": bson.M{
			"classroom_id": kelas,
		}}
		pipe = append(pipe, matchPipe)
	}

	// GROUPING
	groupPipe := bson.M{
		"$group": bson.M{
			"_id": bson.M{"conference_topic": "$conference_topic"},
			"ConferenceDurations": bson.M{
				"$sum": bson.M{
					"$toInt": "$conference_duration",
				},
			},
		},
	}
	pipe = append(pipe, groupPipe)

	// PROJECT
	projectPipe := bson.M{
		"$project": bson.M{
			"_id":                 0,
			"conference_topic":    "$_id.conference_topic",
			"ConferenceDurations": "$ConferenceDurations",
		},
	}
	pipe = append(pipe, projectPipe)

	// SORTING
	sortPipe := bson.M{
		"$sort": bson.M{"conference_topic": 1},
	}
	pipe = append(pipe, sortPipe)

	// AGGREGATOR
	res, err := col.Aggregate(context.TODO(), pipe)
	if err != nil {
		return model.ConferenceLiveReportDataset{}, nil
	}

	for res.Next(context.TODO()) {
		var e entity.Conference
		err := res.Decode(&e)
		if err != nil {
			return model.ConferenceLiveReportDataset{}, err
		}
		label = append(label, e.ConferenceTopic)
		data = append(data, e.ConferenceDurations)
	}

	datasets = append(datasets, model.ConferenceLiveReportDataDetail{
		Labels: "Durasi Video Conference",
		Data:   data,
	})
	return model.ConferenceLiveReportDataset{Labels: label, Datasets: datasets}, nil
}

func (u Conference) LiveReportTableData(col *mongo.Collection, acq *entity.AccessControlQuery, dari string, sampai string, kelas string, mataPelajaran string) ([]model.ConferenceLiveReportTableData, error) {
	var pipe []bson.M
	var result []model.ConferenceLiveReportTableData
	if acq != nil {
		acqPipe := bson.M{"$match": bson.M{acq.Query: acq.ID}}
		pipe = append(pipe, acqPipe)
	}

	// MATCH
	if dari != "" && sampai != "" {
		matchPipe := bson.M{
			"$match": bson.M{
				"conference_start": bson.M{
					"$gte": dari,
					"$lt":  sampai,
				},
			},
		}
		pipe = append(pipe, matchPipe)
	}
	if mataPelajaran != "" {
		matchPipe := bson.M{"$match": bson.M{
			"conference_topic": mataPelajaran,
		}}
		pipe = append(pipe, matchPipe)
	}
	if kelas != "" {
		matchPipe := bson.M{"$match": bson.M{
			"classroom_id": kelas,
		}}
		pipe = append(pipe, matchPipe)
	}

	// GROUPING
	groupPipe := bson.M{
		"$group": bson.M{
			"_id": primitive.ObjectID{},
			"ConferenceDurations": bson.M{
				"$sum": bson.M{
					"$toInt": "$conference_duration",
				},
			},
			"ConferenceTotals": bson.M{
				"$sum": 1,
			},
			"ConferenceUsersTotals": bson.M{
				"$sum": bson.M{
					"$toInt": "$user_conference_count",
				},
			},
			"ConferenceDurationLongest": bson.M{
				"$max": bson.M{
					"$toInt": "$conference_duration",
				},
			},
		},
	}
	pipe = append(pipe, groupPipe)
	res, err := col.Aggregate(context.TODO(), pipe)
	if err != nil {
		return []model.ConferenceLiveReportTableData{}, nil
	}

	// TRANSFORMATOR
	res.Next(context.TODO())
	var e entity.Conference
	err = res.Decode(&e)
	if err != nil {
		return []model.ConferenceLiveReportTableData{}, err
	}
	result = append(result, model.ConferenceLiveReportTableData{
		Key:    "1",
		Type:   "Total Conference",
		Jumlah: e.ConferenceTotals,
	})
	result = append(result, model.ConferenceLiveReportTableData{
		Key:    "2",
		Type:   "Durasi Conference",
		Jumlah: e.ConferenceDurations,
	})
	result = append(result, model.ConferenceLiveReportTableData{
		Key:    "3",
		Type:   "Jumlah Peserta",
		Jumlah: e.ConferenceUsersTotals,
	})
	result = append(result, model.ConferenceLiveReportTableData{
		Key:    "4",
		Type:   "Total Conference",
		Jumlah: e.ConferenceDurationLongest,
	})
	return result, nil
}
