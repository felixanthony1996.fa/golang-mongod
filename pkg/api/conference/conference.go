package conference

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/utl/accesscontrol"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
)

func (u Conference) Create(c echo.Context, req entity.Conference) (entity.Conference, error) {
	return u.udb.Create(u.collection, req)
}

func (u Conference) CreateBulk(c echo.Context, req []entity.Conference) ([]entity.Conference, error) {
	var result []entity.Conference
	for _, v := range req {
		err := u.udb.DeleteByConferenceID(u.collection, v)
		resultDB, err := u.udb.Create(u.collection, v)
		if err != nil {
			return []entity.Conference{}, err
		}
		result = append(result, resultDB)
	}
	return result, nil
}

func (u Conference) List(c echo.Context, p pagination.Pagination) ([]entity.Conference, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return nil, err
	}
	return u.udb.List(u.collection, acq, p)
}

func (u Conference) Delete(c echo.Context, id string) error {
	model, err := u.udb.View(u.collection, id)
	if err != nil {
		return err
	}
	return u.udb.Delete(u.collection, model)
}

func (u Conference) View(c echo.Context, id string) (entity.Conference, error) {
	return u.udb.View(u.collection, id)
}

func (u Conference) LiveReportData(c echo.Context, dari string, sampai string, kelas string, matapelajaran string) (model.ConferenceLiveReportDataset, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return model.ConferenceLiveReportDataset{}, err
	}
	result, err := u.udb.LiveReportData(u.collection, acq, dari, sampai, kelas, matapelajaran)
	if err != nil {
		return model.ConferenceLiveReportDataset{}, err
	}

	var datasets []model.ConferenceLiveReportDataDetail
	return model.ConferenceLiveReportDataset{
		Labels:   []string{"Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"},
		Datasets: append(datasets, result),
	}, nil
}

func (u Conference) LiveReportTableData(c echo.Context, dari string, sampai string, kelas string, matapelajaran string) ([]model.ConferenceLiveReportTableData, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return []model.ConferenceLiveReportTableData{}, err
	}
	result, err := u.udb.LiveReportTableData(u.collection, acq, dari, sampai, kelas, matapelajaran)
	if err != nil {
		return []model.ConferenceLiveReportTableData{}, err
	}
	return result, nil
}

func (u Conference) LiveReportDataByMapel(c echo.Context, dari string, sampai string, kelas string, matapelajaran string) (model.ConferenceLiveReportDataset, error) {
	r := u.rbac.User(c)
	acq, err := accesscontrol.List(r)
	if err != nil {
		return model.ConferenceLiveReportDataset{}, err
	}
	result, err := u.udb.LiveReportDataByMapel(u.collection, acq, dari, sampai, kelas, matapelajaran)
	if err != nil {
		return model.ConferenceLiveReportDataset{}, err
	}
	return result, err
}
