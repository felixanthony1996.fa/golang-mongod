package transporter

import (
	"felixa.com/golangmongod/entity"
)

type createReq struct {
	ConferenceId        string `bson:"conference_id" json:"conference_id" validate:"required"`
	ConferenceTopic     string `bson:"conference_topic" json:"conference_topic" validate:"required"`
	ConferenceStart     string `bson:"conference_start" json:"conference_start"`
	ConferenceDuration  string `bson:"conference_duration" json:"conference_duration"`
	UserConferenceCount string `bson:"user_conference_count" json:"user_conference_count"`
	UseConferenceStatus string `bson:"use_conference_status" json:"use_conference_status"`
	ClassroomId         string `bson:"classroom_id" json:"classroom_id"`
	ClassroomName       string `bson:"classroom_name" json:"classroom_name"`
	TeacherClassId      string `bson:"teacher_class_id" json:"teacher_class_id"`
	SchoolClassId       string `bson:"school_class_id" json:"school_class_id"`
	SchoolClassName     string `bson:"school_class_name" json:"school_class_name"`
	PeriodId            string `bson:"period_id" json:"period_id"`
	Semester            string `bson:"semester" json:"semester"`
	YearId              string `bson:"year_id" json:"year_id" validate:"required"`
	YearName            string `bson:"year_name" json:"year_name"`
	TeacherId           string `bson:"teacher_id" json:"teacher_id"`
	OfficialId          string `bson:"official_id" json:"official_id"`
	TeacherName         string `bson:"teacher_name" json:"teacher_name"`
	TeacherUserName     string `bson:"teacher_user_name" json:"teacher_user_name"`
	SchoolId            string `bson:"school_id" json:"school_id"`
	SchoolName          string `bson:"school_name" json:"school_name"`
	AddressId           string `bson:"address_id" json:"address_id"`
	AddressName         string `bson:"address_name" json:"address_name"`
	CountryId           string `bson:"country_id" json:"country_id" validate:"required"`
	CountryName         string `bson:"country_name" json:"country_name" validate:"required"`
	ProvinceId          string `bson:"province_id" json:"province_id" validate:"required"`
	ProvinceName        string `bson:"province_name" json:"province_name" validate:"required"`
	DistrictId          string `bson:"district_id" json:"district_id" validate:"required"`
	DistrictName        string `bson:"district_name" json:"district_name" validate:"required"`
	SubDistrictId       string `bson:"subdistrict_id" json:"subdistrict_id"`
	SubDistrictName     string `bson:"subdistrict_name" json:"subdistrict_name"`
	VillageId           string `bson:"village_id" json:"village_id"`
	VillageName         string `bson:"village_name" json:"village_name"`
}
type createReqBulk struct {
	Data []createReq `json:"data" validate:"required"`
}

type listResponse struct {
	Conferences []entity.Conference `json:"data" `
	Page        int64               `json:"page"`
}

// Function
func (c *createReq) ToConferenceEntity() entity.Conference {
	return entity.Conference{
		ConferenceId:        c.ConferenceId,
		ConferenceTopic:     c.ConferenceTopic,
		ConferenceStart:     c.ConferenceStart,
		ConferenceDuration:  c.ConferenceDuration,
		UserConferenceCount: c.UserConferenceCount,
		UseConferenceStatus: c.UseConferenceStatus,
		ClassroomId:         c.ClassroomId,
		ClassroomName:       c.ClassroomName,
		TeacherClassId:      c.TeacherClassId,
		SchoolClassId:       c.SchoolClassId,
		SchoolClassName:     c.SchoolClassName,
		PeriodId:            c.PeriodId,
		Semester:            c.Semester,
		YearId:              c.YearId,
		YearName:            c.YearName,
		TeacherId:           c.TeacherId,
		OfficialId:          c.OfficialId,
		TeacherName:         c.TeacherName,
		SchoolId:            c.SchoolId,
		SchoolName:          c.SchoolName,
		AddressId:           c.AddressId,
		AddressName:         c.AddressName,
		CountryId:           c.CountryId,
		CountryName:         c.CountryName,
		ProvinceId:          c.ProvinceId,
		ProvinceName:        c.ProvinceName,
		DistrictId:          c.DistrictId,
		DistrictName:        c.DistrictName,
		SubDistrictId:       c.SubDistrictId,
		SubDistrictName:     c.SubDistrictName,
		VillageId:           c.VillageId,
		VillageName:         c.VillageName,
	}
}
func (c *createReqBulk) ToArrayConferenceEntity() []entity.Conference {
	var result []entity.Conference
	for _, v := range c.Data {
		result = append(result, v.ToConferenceEntity())
	}
	return result
}
