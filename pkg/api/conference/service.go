package conference

import (
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/model"
	"felixa.com/golangmongod/pkg/api/conference/mongod"
	"felixa.com/golangmongod/pkg/utl/shared/helper/pagination"
	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportData(echo.Context, string, string, string, string) (model.ConferenceLiveReportDataset, error)
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportTableData(echo.Context, string, string, string, string) ([]model.ConferenceLiveReportTableData, error)
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportDataByMapel(echo.Context, string, string, string, string) (model.ConferenceLiveReportDataset, error)
	Create(echo.Context, entity.Conference) (entity.Conference, error)
	CreateBulk(echo.Context, []entity.Conference) ([]entity.Conference, error)
	List(echo.Context, pagination.Pagination) ([]entity.Conference, error)
	View(echo.Context, string) (entity.Conference, error)
	Delete(echo.Context, string) error
}

type UDB interface {
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportData(*mongo.Collection, *entity.AccessControlQuery, string, string, string, string) (model.ConferenceLiveReportDataDetail, error)
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportTableData(*mongo.Collection, *entity.AccessControlQuery, string, string, string, string) ([]model.ConferenceLiveReportTableData, error)
	// Parameter:Dari,Sampai,Kelas,MataPelajaran
	LiveReportDataByMapel(*mongo.Collection, *entity.AccessControlQuery, string, string, string, string) (model.ConferenceLiveReportDataset, error)
	Create(*mongo.Collection, entity.Conference) (entity.Conference, error)
	List(*mongo.Collection, *entity.AccessControlQuery, pagination.Pagination) ([]entity.Conference, error)
	Delete(*mongo.Collection, entity.Conference) error
	DeleteByConferenceID(*mongo.Collection, entity.Conference) error
	View(*mongo.Collection, string) (entity.Conference, error)
}

type RBAC interface {
	User(ctx echo.Context) entity.AuthUser
}

type Conference struct {
	collection *mongo.Collection
	udb        UDB
	rbac       RBAC
}

func Initialize(db *mongo.Database, rbac RBAC) *Conference {
	return &Conference{
		collection: db.Collection("conference"),
		udb:        mongod.Conference{},
		rbac:       rbac,
	}
}
