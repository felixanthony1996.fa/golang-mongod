package pagination

const (
	paginationDefaultLimit = 100
	paginationMaxLimit     = 1000
)

type PaginationReq struct {
	Limit int64 `query:"limit"`
	Page  int64 `query:"page" validate:"min=0"`
}

func (p PaginationReq) Transform() Pagination {
	if p.Limit < 1 {
		p.Limit = paginationDefaultLimit
	}

	if p.Limit > paginationMaxLimit {
		p.Limit = paginationMaxLimit
	}

	return Pagination{Limit: p.Limit, Offset: p.Page * p.Limit}
}

type Pagination struct {
	Limit  int64 `json:"limit,omitempty"`
	Offset int64 `json:"offset,omitempty"`
}
