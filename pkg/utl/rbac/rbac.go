package rbac

import (
	"felixa.com/golangmongod/entity"
	"github.com/labstack/echo"
)

type Service struct{}

func (s Service) User(c echo.Context) entity.AuthUser {
	id := c.Get("id").(string)
	user := c.Get("username").(string)
	email := c.Get("email").(string)
	role := c.Get("role").(float64)
	provinceId := c.Get("province_id").(string)
	return entity.AuthUser{
		ID:         id,
		Username:   user,
		Email:      email,
		Role:       role,
		ProvinceID: provinceId,
	}
}
