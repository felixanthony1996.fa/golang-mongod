package accesscontrol

import (
	"felixa.com/golangmongod/entity"
	"github.com/labstack/echo"
)

func List(u entity.AuthUser) (*entity.AccessControlQuery, error) {
	switch true {
	case u.Role <= entity.AdminRole: // Super Admin or Admin doesn't need to filter
		return nil, nil
	case u.Role == entity.ProvinceRole:
		return &entity.AccessControlQuery{Query: "province_id", ID: u.ProvinceID}, nil
	default:
		return nil, echo.ErrForbidden
	}
}
