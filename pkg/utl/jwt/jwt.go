package jwt

import (
	"encoding/json"
	"felixa.com/golangmongod/entity"
	"felixa.com/golangmongod/pkg/utl/errorhandler"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var minSecretLen = 64

func New(algo, secret string, ttlMinutes, minSecretLength int, apiGatewayURL string, apiGatewayConsumer string) (Service, error) {
	if minSecretLength > 0 {
		minSecretLen = minSecretLength
	}
	if len(secret) < minSecretLen {
		return Service{}, fmt.Errorf("Jwt secret length is %v, which is less than required %v", len(secret), minSecretLen)
	}
	signingMethod := jwt.GetSigningMethod(algo)
	if signingMethod == nil {
		return Service{}, fmt.Errorf("Invalid JWT Sign: %s", algo)
	}

	return Service{
		key:                []byte(secret),
		algo:               signingMethod,
		ttl:                time.Duration(ttlMinutes) * time.Minute,
		secret:             secret,
		apiGatewayURL:      apiGatewayURL,
		apiGatewayConsumer: apiGatewayConsumer,
	}, nil
}

type Service struct {
	key                []byte
	ttl                time.Duration
	algo               jwt.SigningMethod
	secret             string
	apiGatewayURL      string
	apiGatewayConsumer string
}

func (s Service) ParseToken(authHeader string) (*jwt.Token, error) {
	parts := strings.SplitN(authHeader, " ", 2)
	if !(len(parts) == 2 && parts[0] == "Bearer") {
		return nil, errorhandler.ErrCommon
	}

	return jwt.Parse(parts[1], func(token *jwt.Token) (interface{}, error) {
		if s.algo != token.Method {
			return nil, errorhandler.ErrCommon
		}
		return s.key, nil
	})

}

// GenerateToken generates new JWT token and populates it with user data
func (s Service) GenerateToken(u entity.User, key string) (string, error) {
	return jwt.NewWithClaims(s.algo, jwt.MapClaims{
		"iss":  key,
		"uuid": u.UUID,
		"u":    u.Username,
		"e":    u.Email,
		"r":    u.Role,
		"p":    u.ProvinceID,
		"exp":  time.Now().Add(s.ttl).Unix(),
	}).SignedString(s.key)

}

// A Response struct to map the Entire Response
type KongApiGatewayResponse struct {
	ConsumerId string `json:"consumer_id"`
	Key        string `json:"key"`
	Secret     string `json:"secret"`
}

func (s Service) GenerateKeyFromGateway(u entity.User) (string, error) {
	if s.apiGatewayURL == "" {
		return "-----------", nil
	}
	response, err := http.PostForm(s.apiGatewayURL+"consumers/"+s.apiGatewayConsumer+"/jwt", url.Values{
		"secret": {s.secret},
		"tags":   {u.Username},
	})
	if err != nil {
		log.Print(err)
		return "", err
	}

	var result KongApiGatewayResponse
	_ = json.NewDecoder(response.Body).Decode(&result)

	return result.Key, nil
}
