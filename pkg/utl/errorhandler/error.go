package errorhandler

import (
	"errors"
	"github.com/labstack/echo"
)

var (
	ErrCommon       = errors.New("Common Error")
	ErrBadRequest   = echo.NewHTTPError(400)
	ErrUnauthorized = echo.ErrUnauthorized
)
