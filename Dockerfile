FROM golang:1.12-alpine
RUN apk add --no-cache git
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN ls
RUN go build -o main .
RUN adduser -S -D -H -h /app appuser
USER appuser
CMD ["./main"]