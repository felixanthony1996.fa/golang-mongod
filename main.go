package main

import (
	"felixa.com/golangmongod/pkg/api"
	config "felixa.com/golangmongod/pkg/utl/config"
	"flag"
)

func main() {
	// TODO change when deploy to production
	cfgPath := flag.String("p", "conf.prod.yaml", "Path to config file")
	flag.Parse()

	cfg, err := config.Load(*cfgPath)
	checkErr(err)

	checkErr(api.Start(cfg))
}

func checkErr(err error) {
	if err != nil {
		panic(err.Error())
	}
}
