package entity

type AccessRole int

const (
	SuperAdminRole float64 = 100

	AdminRole float64 = 110

	// Standard User
	ProvinceRole float64 = 200
	UserRole     float64 = 200
)
