package entity

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Teacher struct {
	ID    		primitive.ObjectID `bson:"_id,omitempty" json:"_id"`
	Todo     string             `json:"todo"`
}

