package entity

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Conference struct {
	ID                  primitive.ObjectID `bson:"_id,omitempty" json:"_id"`
	ConferenceId        string             `bson:"conference_id" json:"conference_id"`
	ConferenceTopic     string             `bson:"conference_topic" json:"conference_topic"`
	ConferenceStart     string             `bson:"conference_start" json:"conference_start"`
	ConferenceDuration  string             `bson:"conference_duration" json:"conference_duration"`
	UserConferenceCount string             `bson:"user_conference_count" json:"user_conference_count"`
	UseConferenceStatus string             `bson:"use_conference_status" json:"use_conference_status"`
	ClassroomId         string             `bson:"classroom_id" json:"classroom_id"`
	ClassroomName       string             `bson:"classroom_name" json:"classroom_name"`
	TeacherClassId      string             `bson:"teacher_class_id" json:"teacher_class_id"`
	SchoolClassId       string             `bson:"school_class_id" json:"school_class_id"`
	SchoolClassName     string             `bson:"school_class_name" json:"school_class_name"`
	PeriodId            string             `bson:"period_id" json:"period_id"`
	Semester            string             `bson:"semester" json:"semester"`
	YearId              string             `bson:"year_id" json:"year_id"`
	YearName            string             `bson:"year_name" json:"year_name"`
	TeacherId           string             `bson:"teacher_id" json:"teacher_id"`
	OfficialId          string             `bson:"official_id" json:"official_id"`
	TeacherName         string             `bson:"teacher_name" json:"teacher_name"`
	TeacherUserName     string             `bson:"teacher_user_name" json:"teacher_user_name"`
	SchoolId            string             `bson:"school_id" json:"school_id"`
	SchoolName          string             `bson:"school_name" json:"school_name"`
	AddressId           string             `bson:"address_id" json:"address_id"`
	AddressName         string             `bson:"address_name" json:"address_name"`
	CountryId           string             `bson:"country_id" json:"country_id"`
	CountryName         string             `bson:"country_name" json:"country_name"`
	ProvinceId          string             `bson:"province_id" json:"province_id"`
	ProvinceName        string             `bson:"province_name" json:"province_name"`
	DistrictId          string             `bson:"district_id" json:"district_id"`
	DistrictName        string             `bson:"district_name" json:"district_name"`
	SubDistrictId       string             `bson:"subdistrict_id" json:"subdistrict_id"`
	SubDistrictName     string             `bson:"subdistrict_name" json:"subdistrict_name"`
	VillageId           string             `bson:"village_id" json:"village_id"`
	VillageName         string             `bson:"village_name" json:"village_name"`
	// Virtual Property
	DayOfWeek int `bson:"dayOfWeek" json:"dayOfWeek"`
	Count     int `bson:"count" json:"count"`
	Sum       int `bson:"sum" json:"sum"`
	// Virtual Property ConferenceLiveReportTableData
	ConferenceTotals int `bson:"ConferenceTotals" json:"ConferenceTotals"`
	ConferenceDurations int `bson:"ConferenceDurations" json:"ConferenceDurations"`
	ConferenceUsersTotals int `bson:"ConferenceUsersTotals" json:"ConferenceUsersTotals"`
	ConferenceDurationLongest int `bson:"ConferenceDurationLongest" json:"ConferenceDurationLongest"`
}
