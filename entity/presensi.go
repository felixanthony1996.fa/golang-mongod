package entity

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Presensi struct {
	ID                       primitive.ObjectID `bson:"_id,omitempty" json:"_id"`
	PresensiId               string             `bson:"presensi_id" json:"presensi_id"`
	PresensiTopic            string             `bson:"presensi_topic" json:"presensi_topic"`
	PresensiDate             string             `bson:"presensi_date" json:"presensi_date"`
	PresensiTime             string             `bson:"presensi_time" json:"presensi_time"`
	CountStudent             int                `bson:"count_student" json:"count_student"`
	CountPresentStudent      int                `bson:"count_present_student" json:"count_present_student"`
	CountAbsenStudent        int                `bson:"count_absen_student" json:"count_absen_student"`
	CountSickStudent         int                `bson:"count_sick_student" json:"count_sick_student"`
	CountPermitStudent       int                `bson:"count_permit_student" json:"count_permit_student"`
	CountDispensationStudent int                `bson:"count_dispensation_student" json:"count_dispensation_student"`
	SubjectName              string             `bson:"subject_name" json:"subject_name"`
	SchoolClassName          string             `bson:"school_class_name" json:"school_class_name"`
	Semester                 string             `bson:"semester" json:"semester"`
	YearName                 string             `bson:"year_name" json:"year_name"`
	TeacherName              string             `bson:"teacher_name" json:"teacher_name"`
	TeacherUserName          string             `bson:"teacher_user_name" json:"teacher_user_name"`
	SchoolName               string             `bson:"school_name" json:"school_name"`
	AddressName              string             `bson:"address_name" json:"address_name"`
	CountryId                string             `bson:"country_id" json:"country_id"`
	CountryName              string             `bson:"country_name" json:"country_name"`
	ProvinceId               string             `bson:"province_id" json:"province_id"`
	ProvinceName             string             `bson:"province_name" json:"province_name"`
	DistrictId               string             `bson:"district_id" json:"district_id"`
	DistrictName             string             `bson:"district_name" json:"district_name"`
	SubDistrictId            string             `bson:"subdistrict_id" json:"subdistrict_id"`
	SubDistrictName          string             `bson:"subdistrict_name" json:"subdistrict_name"`
	VillageId                string             `bson:"village_id" json:"village_id"`
	VillageName              string             `bson:"village_name" json:"village_name"`
	// Virtual Property
	DayOfWeek int `bson:"dayOfWeek" json:"dayOfWeek"`
	Count     int `bson:"count" json:"count"`
	Sum       int `bson:"sum" json:"sum"`
	// Virtual Property PresensiLiveReportTableData
	TotalPresensi   int     `bson:"TotalPresensi" json:"TotalPresensi"`
	AvgPresent      float64 `bson:"AvgPresent" json:"AvgPresent"`
	AvgAbsent       float64 `bson:"AvgAbsent" json:"AvgAbsent"`
	AvgSick         float64 `bson:"AvgSick" json:"AvgSick"`
	AvgPermit       float64 `bson:"AvgPermit" json:"AvgPermit"`
	AvgDispensation float64 `bson:"AvgDispensation" json:"AvgDispensation"`
}
