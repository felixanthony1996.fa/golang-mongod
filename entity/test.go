package entity

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Test struct {
	ID              primitive.ObjectID `bson:"_id,omitempty" json:"_id"`
	TestId          string             `bson:"test_id" json:"test_id"`
	TestTopic       string             `bson:"test_topic" json:"test_topic"`
	TestStart       string             `bson:"test_start" json:"test_start"`
	TestRange       int                `bson:"test_range" json:"test_range"`
	CountExaminee   int                `bson:"count_examinee" json:"count_examinee"`
	MinScore        int                `bson:"min_score" json:"min_score"`
	MaxScore        int                `bson:"max_score" json:"max_score"`
	AvgScore        int                `bson:"avg_score" json:"avg_score"`
	SubjectName     string             `bson:"subject_name" json:"subject_name"`
	SchoolClassName string             `bson:"school_class_name" json:"school_class_name"`
	LevelName       int                `bson:"level_name" json:"level_name"`
	MajorName       string             `bson:"major_name" json:"major_name"`
	Semester        string             `bson:"semester" json:"semester"`
	YearName        string             `bson:"year_name" json:"year_name"`
	TeacherName     string             `bson:"teacher_name" json:"teacher_name"`
	TeacherUserName string             `bson:"teacher_user_name" json:"teacher_user_name"`
	SchoolName      string             `bson:"school_name" json:"school_name"`
	StageName       string             `bson:"stage_name" json:"stage_name"`
	AddressName     string             `bson:"address_name" json:"address_name"`
	Country         string             `bson:"country" json:"country"`
	Province        string             `bson:"province" json:"province"`
	District        string             `bson:"district" json:"district"`
	Subdistrict     string             `bson:"subdistrict" json:"subdistrict"`
	Village         string             `bson:"village" json:"village"`
	// Virtual Property
	Count int `bson:"count" json:"count"`
}
