package entity

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type User struct {
	ID                 primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	UUID               string             `json:"uuid"`
	Name               string             `json:"name"`
	Username           string             `json:"username"`
	Password           string             `json:"-"`
	Email              string             `json:"email"`
	Active             bool               `json:"active"`
	LastLogin          time.Time          `json:"last_login,omitempty",bsonType:"DateTime"`
	LastPasswordChange time.Time          `json:"last_password_change,omitempty"`
	Token              string             `json:"-"`
	Role               int                `json:"role",bson:"role"`
	ProvinceID         string             `json:"province_id",bson:"province_id"`
}

// AuthUser stand for data stored in JWT Token
type AuthUser struct {
	ID         string
	Username   string
	Email      string
	Role       float64
	ProvinceID string
}

// Function
func (u *User) UpdateLastLogin(token string) {
	u.Token = token
	u.LastLogin = time.Now()
}
