# GOLANG MONGOD BOILERPLATE


`**Echo + MongoDB + JWT + RBAC**`

**For the development we are using nodemon from NodeJS to ensure the app is hot reloading**
If you never install nodemon.
You must install it first by running command `npm install -g nodemon`
It will try to install globally so you can use it to another project without having too install nodemon again

**Starting the development server**

`nodemon --watch './**/*.go' --signal SIGTERM --exec 'go' run main.go`

**Build the production server**

`go build`

**Starting the production server**

`go run main.go`


### Add this account to your *MongoDB*
    {  
       "name":"John Doe",
       "username":"johndoe",
       "password":"Password",
       "email":"johndoe@mail.com",
       "active":true,
       "token":"",
       "role":100 (SuperAdmin), 200(Province Admin),
       "last_login":{"$date":"2020-07-19T20:20:39.488Z"},
       "uuid":"UUIDV4",
       "province_id":""
    }

### Login
- http://localhost:8080/login

Request Body


    {
          "username" : "xxxx",
          "password" : "xxxx"
    }
-------------
### Required Login (Send the Authorization in header)
>       Authorization: Bearer xxx
- GET   http://localhost:8080/whoami
- GET   http://localhost:8080/v1/users
- POST  http://localhost:8080/v1/users
- GET   http://localhost:8080/v1/conferences/live-report
- GET   http://localhost:8080/v1/conferences?limit=1000
- POST  http://localhost:8080/v1/conferences

 brew services run mongodb-community