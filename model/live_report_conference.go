package model

// DATASETS
type ConferenceLiveReportDataDetail struct {
	Labels string `json:"labels"`
	Data   []int  `json:"series"`
}

type ConferenceLiveReportDataset struct {
	Labels   []string                         `json:"labels"`
	Datasets []ConferenceLiveReportDataDetail `json:"datasets"`
}

// TABLE DATA
type ConferenceLiveReportTableData struct {
	Key    string `json:"key"`
	Type   string `json:"type"`
	Jumlah int    `json:"jumlah"`
}

// LIVE REPORT
type ConferenceLiveReport struct {
	Data      ConferenceLiveReportDataset     `json:"data"`
	TableData []ConferenceLiveReportTableData `json:"table_data"`
}

type ConferenceLiveReportByMapel struct {
	Data ConferenceLiveReportDataset `json:"data"`
}
