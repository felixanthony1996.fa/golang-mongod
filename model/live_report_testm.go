package model

// LIVE REPORT TEST
type TestLiveReportData struct {
	Labels []string `json:"labels"`
	Series [][]int  `json:"series"`
}

type TestLiveReport struct {
	Data TestLiveReportData `json:"data"`
}

// LIVE REPORT TEST AVERAGE
type TestAverageLiveReportDataDetail struct {
	Labels string `json:"labels"`
	Data   []int  `json:"series"`
}

type TestAverageLiveReportDataset struct {
	Labels   []string                          `json:"labels"`
	Datasets []TestAverageLiveReportDataDetail `json:"datasets"`
}

type TestAverageLiveReport struct {
	Data TestAverageLiveReportDataset `json:"data"`
}
