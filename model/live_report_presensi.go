package model

// LIVE REPORT PRESENSI
type PresensiLiveReportTableData struct {
	Key    string  `json:"key"`
	Type   string  `json:"type"`
	Jumlah float64 `json:"jumlah"`
}

type PresensiLiveReportData struct {
	Labels []string `json:"labels"`
	Series [][]int  `json:"series"`
}

type PresensiLiveReport struct {
	Data      PresensiLiveReportData        `json:"data"`
	TableData []PresensiLiveReportTableData `json:"table_data"`
}
